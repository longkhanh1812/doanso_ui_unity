﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoanSo : MonoBehaviour
{

    int max,min;
    int guess;
    int cmp_ua,cmp_da;
    public Text guessText;
    void StartGame()
    {
        print("--------------------");
        max=10000;
        min=1000;
        guess=(max+min)/2;
        print("-version 1.0-");
        print("So lon nhat" + max);
        print("So nho nhat" + min);
        print("---------------------");
        print("Mui ten xuong va len");
        
    }
    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }
    public int random_cmp_ua()
    {
        return Random.Range(guess,max);
    }
    public int random_cmp_da()
    {
        return Random.Range(min,guess);
    }

    public void get_higher()
    {
        cmp_ua = random_cmp_ua();
        guessText.text = cmp_ua.ToString();
    }
    public void get_smaller()
    {
        cmp_da = random_cmp_da();
        guessText.text = cmp_da.ToString();  
    }
    public void get_equals()
    {
        guessText.text=guess.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            cmp_ua = random_cmp_ua();
            guessText.text = cmp_ua.ToString();
        }else if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            cmp_da = random_cmp_da();
            guessText.text = cmp_da.ToString();  
        }
    }
}
